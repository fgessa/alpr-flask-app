import os
import tempfile


class BaseConfig:
    TESTING = False
    SECRET_KEY = "my-secret-key"
    DOCKER_HOST_IP = os.getenv("DOCKER_HOST_IP", "localhost")
    DETECTION_MODEL_URI = f"http://{DOCKER_HOST_IP}:8502/v1/models/lp_detector:predict"
    OCR_MODEL_URI = f"http://{DOCKER_HOST_IP}:8503/v1/models/lp_ocr:predict"
    MAX_CONTENT_LENGTH = 8 * 1024 * 1024
    DETECTION_MIN_CONFIDENCE = 0.9
    UPLOADS_DIR = "static/uploads"
    ALLOWED_EXTENSIONS = {"jpg", "jpeg", "png"}
    HEADERS = {"content-type":"application/json"}


class DevelopmentConfig(BaseConfig):
    pass


class TestingConfig(BaseConfig):
    TESTING = True
