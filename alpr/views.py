import os
import random
import string

import cv2
from flask import current_app as app
from flask import request, render_template, redirect, flash
from werkzeug.utils import secure_filename

from alpr.pipeline.recognition import recognize_plates


def is_allowed_file(filename):
    ext = filename[filename.rfind(".") + 1 :]
    return ext.lower() in app.config["ALLOWED_EXTENSIONS"]


def generate_filename():
    chars = string.ascii_letters
    return "".join(random.choices(chars, k=10)) + ".jpg"


@app.route("/upload", methods=["GET", "POST"])
def upload():
    if request.method == "POST":
        file = request.files["file"]
        if file.filename == "":
            flash("No file was uploaded")
            return redirect(request.url)
        if not is_allowed_file(file.filename):
            flash("Not allowed file type. Please upload an image")
            return redirect(request.url)
        if file and is_allowed_file(file.filename):
            filename = secure_filename(file.filename)
            image_path = os.path.sep.join(
                [app.root_path, app.config["UPLOADS_DIR"], filename]
            )
            file.save(image_path)

        prediction = recognize_plates(image_path)
        if not prediction.success:
            flash("No license plates detected in image")
            return redirect(request.url)

        random_filename = generate_filename()
        p = os.path.sep.join(
            [app.root_path, app.config["UPLOADS_DIR"], random_filename]
        )
        cv2.imwrite(p, prediction.image)
        cv2.waitKey(0)

        return render_template(
            "prediction.html",
            image_path=os.path.sep.join(
                [app.config["UPLOADS_DIR"], random_filename]
            ),
            predictions=zip(prediction.texts, prediction.seq_conf_scores),
        )

    return render_template("upload.html")
