import os

from flask import Flask


# instatiate app
app = Flask(__name__)

# load configurations
app_settings = os.getenv("APP_SETTINGS", "config.DevelopmentConfig")
app.config.from_object(app_settings)

with app.app_context():
    # import views
    from . import views
