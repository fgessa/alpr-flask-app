import cv2
import imutils
import numpy as np


def get_interpolation_method(image_size, target_image_size):
    # select interpolation method based on whether
    # the image will be shrinked or stretched
    if (
        image_size[0] > target_image_size[0]
        or image_size[1] > target_image_size[1]
    ):
        inter = cv2.INTER_AREA
    else:
        inter = cv2.INTER_CUBIC

    return inter


def load_image(image_path, size=(800, 800)):
    image = cv2.imread(image_path)
    (h, w) = image.shape[:2]

    inter = get_interpolation_method((w, h), target_image_size=size)

    # check to see if we should resize along the width
    if w > h and w > size[0]:
        image = imutils.resize(image, width=size[0], inter=inter)

    # otherwise, check to see if we should resize along
    # the height
    elif h > w and h > size[1]:
        image = imutils.resize(image, height=size[1], inter=inter)

    return image


def prepare_image(image):
    image = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2RGB)
    image = np.expand_dims(image, axis=0)
    return image


def resize_and_pad(image, size=(200, 200), value=[0]):
    # initialize the variables to hold the difference between
    # the new size (of resized image) and target size
    padH = 0
    padW = 0

    # get the shape of image
    h, w = image.shape[:2]

    inter = get_interpolation_method((w, h), target_image_size=size)

    # resize along the size[1] if its is less than
    # the size[1]
    if w > h:
        resized = imutils.resize(image, width=size[0], inter=inter)
        padH = int((size[1] - resized.shape[0]) / 2)

    # otherwise resize along the size[1]
    else:
        resized = imutils.resize(image, height=size[1], inter=inter)
        padW = int((size[0] - resized.shape[1]) / 2)

    # initiailize value to match image channel
    value = value * resized.shape[-1]
    padded = cv2.copyMakeBorder(
        resized, padH, padH, padW, padW, cv2.BORDER_CONSTANT, value=value
    )

    # resize the image again before incase there are a few pixels off
    return cv2.resize(padded, size, interpolation=inter)


def draw_bboxes_with_labels(
    orig_image_path,
    plate_bboxes,
    plate_labels,
    conf_scores,
    show_conf_scores=False,
):
    # box color
    color = (250, 0, 250)

    # initialize font color, scale and text_size
    font_color = (255, 255, 255)
    font_scale = 0.5
    text_size = 1

    image = load_image(orig_image_path, size=(800, 800))

    # coordinates and label
    for (bbox, label, conf_score) in zip(
        plate_bboxes, plate_labels, conf_scores
    ):
        # extract bounding box coordinates
        start_x, start_y, end_x, end_y = bbox

        # draw bounding box around the  license plate
        image = cv2.rectangle(
            image, (start_x, start_y), (end_x, end_y), color, 2
        )

        # combine label with scores
        if show_conf_scores:
            label = " ".join([label, "{}%".format(int(conf_score * 100))])

        # use opencv's getTextSize to compute the size of the label
        (text_width, text_height), baseline = cv2.getTextSize(
            label, cv2.FONT_HERSHEY_SIMPLEX, font_scale, text_size
        )
        # draw backgound box onto which the label is rendered.
        image = cv2.rectangle(
            image,
            (start_x, start_y - text_height - baseline),
            (start_x + text_width, start_y),
            color,
            thickness=cv2.FILLED,
        )
        # write label text onto background box
        image = cv2.putText(
            image,
            label,
            (start_x, start_y - baseline),
            cv2.FONT_HERSHEY_SIMPLEX,
            font_scale,
            font_color,
            text_size,
        )

    # return rendered image
    return image
