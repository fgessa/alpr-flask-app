(function() {
    
    // get the predict button
    const predictButton = document.getElementById("predict");
    // get the file input element
    const fileInput = document.querySelector('#image-file input[type=file]');

    // add is loading class to element when it is clicked
    predictButton.onclick = () => {
        predictButton.classList.add("is-loading");
    }

    // display name of uploaded file    
    fileInput.onchange = () => {
      if (fileInput.files.length > 0) {
        const fileName = document.querySelector('#image-file .file-name');
        fileName.textContent = fileInput.files[0].name;
      }
    }

})();