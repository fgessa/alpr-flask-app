import collections

from flask import current_app as app

from alpr.pipeline.detection import detect_plates
from alpr.pipeline.ocr import read_plates
from alpr.utils.alpr_utils import draw_bboxes_with_labels, load_image

Prediction = collections.namedtuple(
    "Prediction", ["image", "texts", "seq_conf_scores", "success"]
)
Prediction.__new__.__defaults__ = (None, None, None, False)


def recognize_plates(image_path):
    image = load_image(image_path)
    lp_detection = detect_plates(image, app.config["DETECTION_MIN_CONFIDENCE"])

    if not lp_detection.success:
        return Prediction()
    else:
        predicted_texts = []
        seq_conf_scores = []
        for patch in lp_detection.patches:
            predicted_text, seq_conf = read_plates(patch)
            predicted_texts.append(predicted_text)
            seq_conf_scores.append(seq_conf)

        output_image = draw_bboxes_with_labels(
            image_path, lp_detection.bboxes, predicted_texts, seq_conf_scores
        )

        return Prediction(
            image=output_image,
            texts=predicted_texts,
            seq_conf_scores=seq_conf_scores,
            success=lp_detection.success,
        )
