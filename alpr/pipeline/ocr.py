import json
import re

import requests
from flask import current_app as app

from alpr.utils import alpr_utils


def read_plates(lp_patch):
    lp_patch = alpr_utils.resize_and_pad(lp_patch)
    lp_patch = alpr_utils.prepare_image(lp_patch)

    data = json.dumps({"instances": lp_patch.tolist()})
    json_response = requests.post(
        app.config["OCR_MODEL_URI"], data=data, headers=app.config["HEADERS"]
    )
    predictions = json.loads(json_response.text)["predictions"]
    predicted_text = predictions[0]["predicted_text"]
    seq_conf = predictions[0]["normalized_seq_conf"]

    return re.sub(r"[^A-Z0-9\s]", "", predicted_text), seq_conf
