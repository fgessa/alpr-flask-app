import collections
import json

import requests
from flask import current_app as app

from alpr.utils import alpr_utils

LPDetection = collections.namedtuple(
    "LPDetection", ["patches", "bboxes", "num_detections", "success"]
)
LPDetection.__new__.__defaults__ = ([], [], 0, False)


def rescale_bbox(bbox, W, H):
    (start_y, start_x, end_y, end_x) = bbox
    start_x = int(start_x * W)
    start_y = int(start_y * H)
    end_x = int(end_x * W)
    end_y = int(end_y * H)
    return (start_x, start_y, end_x, end_y)


def get_detections(image):
    data = json.dumps({"instances": image.tolist()})
    json_response = requests.post(
        app.config["DETECTION_MODEL_URI"],
        data=data,
        headers=app.config["HEADERS"],
    )
    predictions = json.loads(json_response.text)["predictions"]
    bboxes = predictions[0]["detection_boxes"]
    scores = predictions[0]["detection_scores"]
    return (bboxes, scores)


def detect_plates(image, min_confidence=0.5):
    (H, W) = image.shape[:2]
    prepared_image = alpr_utils.prepare_image(image)

    (bboxes, scores) = get_detections(prepared_image)

    plate_patches = []
    plate_bboxes = []
    num_detections = 0

    for (bbox, score) in zip(bboxes, scores):
        if score < min_confidence:
            continue

        (start_x, start_y, end_x, end_y) = rescale_bbox(bbox, W, H)

        plate_patches.append(image[start_y:end_y, start_x:end_x])
        plate_bboxes.append((start_x, start_y, end_x, end_y))
        num_detections += 1

    if not num_detections > 0:
        # return default license plate detection instance
        return LPDetection()
    else:
        # return license plate detection instance containing all detections
        return LPDetection(
            patches=plate_patches,
            bboxes=plate_bboxes,
            num_detections=num_detections,
            success=True,
        )
