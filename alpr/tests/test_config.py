def test_testing_config(test_app):
    test_app.config.from_object("config.TestingConfig")
    assert test_app.config["SECRET_KEY"] == "my-secret-key"
    assert test_app.config["TESTING"]


def test_development_config(test_app):
    test_app.config.from_object("config.DevelopmentConfig")
    assert test_app.config["SECRET_KEY"] == "my-secret-key"
    assert not test_app.config["TESTING"]
