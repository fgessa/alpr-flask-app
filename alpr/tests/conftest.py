import io

import numpy as np
import pytest
from PIL import Image

from alpr import app


@pytest.fixture(scope="module")
def test_app():
    app.config.from_object("config.TestingConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="module")
def fake_image(size=(500, 500)):
    io_memory_file = io.BytesIO()
    image = Image.new(
        "RGB",
        size=size,
        color=tuple(np.random.randint(low=0, high=255, size=3)),
    )
    image.save(io_memory_file, "jpeg")
    io_memory_file.seek(0)
    pil_image = Image.open(io_memory_file)
    return np.array(pil_image)
