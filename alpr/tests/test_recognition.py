import tempfile

import pytest

import alpr.pipeline.recognition
from alpr.pipeline.recognition import recognize_plates


@pytest.fixture
def load_image(fake_image, monkeypatch):
    def mock_load_image(image):
        return fake_image

    monkeypatch.setattr(
        alpr.pipeline.recognition, "load_image", mock_load_image
    )


class LPDetection:
    def __init__(self, patches=None, bboxes=None, success=False):
        self.success = success
        self.patches = patches
        self.bboxes = bboxes


def test_recognize_plates_returns_no_predictions(
    test_app, fake_image, load_image, monkeypatch
):
    def mock_detect_plates(image, min_detection_confidence):
        return LPDetection()

    monkeypatch.setattr(
        alpr.pipeline.recognition, "detect_plates", mock_detect_plates
    )

    prediction = recognize_plates(tempfile.mktemp())

    assert not prediction.image
    assert not prediction.texts
    assert not prediction.seq_conf_scores
    assert not prediction.success


def test_recognize_plates_returns_predictions(
    test_app, fake_image, load_image, monkeypatch
):
    def mock_detect_plates(image, min_detection_confidence):
        return LPDetection(patches=[fake_image], bboxes=[[]], success=True)

    def mock_read_plates(image):
        return "UAX 311E", 0.7

    def mock_draw_bboxes_with_labels(*args, **kwargs):
        return fake_image

    monkeypatch.setattr(
        alpr.pipeline.recognition, "detect_plates", mock_detect_plates
    )
    monkeypatch.setattr(
        alpr.pipeline.recognition, "read_plates", mock_read_plates
    )
    monkeypatch.setattr(
        alpr.pipeline.recognition,
        "draw_bboxes_with_labels",
        mock_draw_bboxes_with_labels,
    )

    prediction = recognize_plates(tempfile.mktemp())

    assert prediction.image is not None
    assert prediction.texts == ["UAX 311E"]
    assert prediction.seq_conf_scores == [0.7]
    assert prediction.success
