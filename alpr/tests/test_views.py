import io
import os

import pytest
from PIL import Image

import alpr.views


def test_upload_page(test_app):
    """
    GIVEN app is initialized and running
    WHEN home page is accessed
    THEN check if home page loads successfully
    """
    client = test_app.test_client()
    resp = client.get("/upload")

    resp_data = resp.data.decode()

    assert resp.status_code == 200
    assert "ALPR for Uganda license plates" in resp_data


def test_upload_invalid_file_type(test_app):
    """
    GIVEN app is initialized and running
    WHEN an invalid file type is uploaded
    THEN reject file
    """
    filename = "not_image_file.txt"
    data = {"file": (io.BytesIO(b""), filename)}
    client = test_app.test_client()
    resp = client.post("/upload", data=data, follow_redirects=True)
    resp_data = resp.data.decode()

    assert "Not allowed file type. Please upload an image" in resp_data


def test_upload_no_file(test_app):
    """
    GIVEN app is initialized and running
    WHEN no file is uploaded on submit
    THEN not file uploaded message is returned
    """
    data = {"file": (io.BytesIO(b""), "")}
    client = test_app.test_client()
    resp = client.post("/upload", data=data, follow_redirects=True)
    resp_data = resp.data.decode()

    assert "No file was uploaded" in resp_data


def test_image_file_contains_no_plates(test_app, monkeypatch):
    """
    GIVEN app is initialized and running
    WHEN  an image file containing no license plates
          is uploaded
    THEN  no plates detected message is displayed
    """

    def mock_recognize_plates(image_path):
        class Prediction:
            def __init__(self):
                self.success = False

        return Prediction()

    def mock_is_allowed_file(filename):
        return True

    monkeypatch.setattr(alpr.views, "recognize_plates", mock_recognize_plates)
    monkeypatch.setattr(alpr.views, "is_allowed_file", mock_is_allowed_file)

    client = test_app.test_client()
    resp = client.post(
        "/upload",
        data={"file": (io.BytesIO(b""), "image.jpg")},
        follow_redirects=True,
    )
    resp_data = resp.data.decode()

    assert "No license plates detected in image" in resp_data


def test_image_file_contains_plates(test_app, fake_image, monkeypatch):
    """
    GIVEN app is initialized and running
    WHEN  an image file containing license plates
          is uploaded
    THEN  display license plates predictions
    """

    def mock_recognize_plates(image_path):
        class Prediction:
            def __init__(self):
                self.image = fake_image
                self.texts = ["UAX 311E"]
                self.seq_conf_scores = [0.7]
                self.success = True

        return Prediction()

    def mock_is_allowed_file(filename):
        return True

    monkeypatch.setattr(alpr.views, "recognize_plates", mock_recognize_plates)
    monkeypatch.setattr(alpr.views, "is_allowed_file", mock_is_allowed_file)
    client = test_app.test_client()
    resp = client.post(
        "/upload",
        data={"file": (io.BytesIO(b""), "image.jpg")},
        follow_redirects=True,
    )
    resp_data = resp.data.decode()

    assert resp.status_code == 200
    assert "UAX 311E" in resp_data
