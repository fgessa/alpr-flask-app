import json

import pytest
import requests

from alpr.pipeline.detection import detect_plates, get_detections, rescale_bbox


class MockDetectionResponse:
    def __init__(self, boxes, scores):
        self.status_Code = 201
        self.boxes = boxes
        self.scores = scores

    @property
    def text(self):
        response_dict = {}
        response_dict["predictions"] = [[]]
        response_dict["predictions"][0] = {"detection_boxes": self.boxes}
        response_dict["predictions"][0].update(
            {"detection_scores": self.scores}
        )
        return json.dumps(response_dict)


@pytest.fixture
def mock_detection_response(monkeypatch, request):
    """
    Returns response with or without any detections based on
    the arguments passed to the MockDetectionResponse class
    """

    def mock_post(url, data, headers):
        return MockDetectionResponse(request.param[0], request.param[1])

    monkeypatch.setattr(requests, "post", mock_post)


@pytest.mark.parametrize("mock_detection_response", [([], [])], indirect=True)
def test_get_detections_returns_no_detections(
    test_app, fake_image, mock_detection_response
):

    boxes, scores = get_detections(fake_image)

    assert boxes == []
    assert scores == []


@pytest.mark.parametrize(
    "mock_detection_response",
    [([[0.3, 0.3, 0.2, 0.4], [0.3, 0.6, 0.2, 0.5]], [0.6, 0.8])],
    indirect=True,
)
def test_get_detections_returns_detections(
    test_app, fake_image, mock_detection_response
):

    boxes, scores = get_detections(fake_image)

    assert boxes == [[0.3, 0.3, 0.2, 0.4], [0.3, 0.6, 0.2, 0.5]]
    assert scores == [0.6, 0.8]


@pytest.mark.parametrize("mock_detection_response", [([], [])], indirect=True)
def test_detect_plates_returns_no_detections(
    test_app, fake_image, mock_detection_response
):

    lp_detection = detect_plates(fake_image)

    expected_bboxes = []
    expected_num_detections = 0

    assert lp_detection.bboxes == expected_bboxes
    assert lp_detection.num_detections == expected_num_detections
    assert not lp_detection.success


@pytest.mark.parametrize(
    "mock_detection_response",
    [([[0.3, 0.3, 0.2, 0.4], [0.3, 0.6, 0.2, 0.5]], [0.6, 0.8])],
    indirect=True,
)
def test_detect_plates_returns_detections(
    test_app, fake_image, mock_detection_response
):
    W, H = fake_image.shape[0], fake_image.shape[1]
    lp_detection = detect_plates(fake_image)

    expected_bboxes = [
        rescale_bbox(bbox, W, H) for bbox in get_detections(fake_image)[0]
    ]
    expected_num_detections = 2

    assert lp_detection.bboxes == expected_bboxes
    assert lp_detection.num_detections == expected_num_detections
    assert lp_detection.success


@pytest.mark.parametrize(
    "bbox, width, height, expected_bbox",
    [
        ((0, 0, 0, 0), 50, 50, (0, 0, 0, 0)),
        ((0.2, 0.2, 0.2, 0.2), 50, 50, (10, 10, 10, 10)),
        ((0.5, 0.2, 0.6, 0.3), 100, 100, (20, 50, 30, 60)),
    ],
)
def test_rescale_bboxes(bbox, width, height, expected_bbox):

    bbox = rescale_bbox(bbox, width, height)

    assert bbox == expected_bbox
