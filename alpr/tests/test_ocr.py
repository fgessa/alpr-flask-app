import json

import pytest
import requests

from alpr.pipeline.ocr import read_plates


class MockOCRResponse:
    def __init__(self, predicted_text, seq_conf):
        self.status_code = 201
        self.predicted_text = predicted_text
        self.seq_conf = seq_conf

    @property
    def text(self):
        response_dict = {}
        response_dict["predictions"] = [[]]
        response_dict["predictions"][0] = {
            "predicted_text": self.predicted_text
        }
        response_dict["predictions"][0].update(
            {"normalized_seq_conf": self.seq_conf}
        )
        return json.dumps(response_dict)


@pytest.fixture
def mock_ocr_response(monkeypatch, request):
    """
    Returns response with both predictions i.e. text and sequence
    confidence based on the arguments passed to the
    MockOCRResponse class
    """

    def mock_post(url, data, headers):
        return MockOCRResponse(request.param[0], request.param[1])

    monkeypatch.setattr(requests, "post", mock_post)


@pytest.mark.parametrize(
    "mock_ocr_response", [("UAX 587P", 0.7)], indirect=True
)
def test_read_plates_returns_predictions(
    test_app, fake_image, mock_ocr_response
):

    pred_text, seq_conf = read_plates(fake_image)

    assert pred_text == "UAX 587P"
    assert seq_conf == 0.7
